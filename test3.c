#include<stdio.h>
float input()
{
	float r;
	printf("enter the value of radius\n");
	scanf("%f",&r);
	return r;
}
float area(float r)
{
	float a=3.14*r*r;
	return a;
}
void display(float r,float a)
{
	printf("the area of the circle having the radius %f is %f",r,a);
}
int main()
{
	float r,a;
	r=input();
	a=area(r);
	display(r,a);
	return 0;
}
