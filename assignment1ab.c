//1a. Write a program to find the largest of three numbers using ternary operator.
#include<stdio.h>
int main()
{
int a,b,c,large;
printf("enter the three numbers a,b,c\n");
scanf("%d%d%d",&a,&b,&c);
large=(a>b)?(((a>c)?a:c):((b>c)?b:c);
printf("largest of three numbers is %d",&large);
return 0;
}