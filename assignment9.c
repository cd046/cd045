//program to read and print 2d array
#include<stdio.h>
int main()
{
	int i,j,m,n,abc[10][10];
	printf("enter the number of rows and coloumns\n");
	scanf("%d%d",&m,&n);
	printf("enter the elements of the 2-d array : \n");
	for (i=0;i<m;i++)
		for (j=0;j<n;j++)
		{
			printf("abc[%d][%d] = ",i,j);
			scanf("%d",&abc[i][j]);
		}
	printf("the given 2-d array is : \n");
	for (i=0;i<m;i++)
		for (j=0;j<n;j++)
		{
			printf("%d\t",abc[i][j]);
			if (j==n-1)
			{
				printf("\n");
			}}
	return 0;
}
